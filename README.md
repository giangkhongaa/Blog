# README
Hướng dẫn các bước cần thiết để cấu hình chạy ứng dụng

* Ruby version: 2.3.3p222
* Rails version: 5.1.3

* Configuration
- Mở file config\application.yml, thay đổi thông tin servers, mail server, facebook api 

* Database creation
- Mở terminal ở source và chạy "rails db:migrate" để tạo Database
- Tiếp tục chạy lệnh "rails server" để chạy server, truy cập trang "http://localhost:3000/" để xem chương trình
