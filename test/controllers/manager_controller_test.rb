require 'test_helper'

class ManagerControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get manager_index_url
    assert_response :success
  end

  test "should get destroy:delete" do
    get manager_destroy:delete_url
    assert_response :success
  end

  test "should get update:put" do
    get manager_update:put_url
    assert_response :success
  end

end
