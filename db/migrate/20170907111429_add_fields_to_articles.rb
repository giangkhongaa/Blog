class AddFieldsToArticles < ActiveRecord::Migration[5.1]
  def change
    add_column :articles, :articletag_id, :bigint
    add_column :articles, :like_id, :bigint
  end
end
