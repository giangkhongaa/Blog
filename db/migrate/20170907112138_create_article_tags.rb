class CreateArticleTags < ActiveRecord::Migration[5.1]
  def change
    create_table :article_tags do |t|
      t.bigint :tag_id
      t.bigint :article_id

      t.timestamps
    end
  end
end
