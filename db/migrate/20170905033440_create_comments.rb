class CreateComments < ActiveRecord::Migration[5.1]
  def change
    create_table :comments do |t|
      t.string :user_id
      t.text :body
      t.bigint :article_id

      t.timestamps default: -> { 'CURRENT_TIMESTAMP' }
    end
  end
end
