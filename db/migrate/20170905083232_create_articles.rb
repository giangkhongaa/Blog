class CreateArticles < ActiveRecord::Migration[5.1]
  def change
    create_table :articles do |t|
      t.string :title
      t.text :text
      t.text :image
      t.bigint :user_id

      t.timestamps default: -> { 'CURRENT_TIMESTAMP' }
    end
  end
end
