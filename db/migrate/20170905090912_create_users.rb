class CreateUsers < ActiveRecord::Migration[5.1]
  def change
    create_table :users do |t|
      t.string :user_name
      t.date :birthday
      t.text :image
      t.string :first_name
      t.string :last_name

      t.timestamps default: -> { 'CURRENT_TIMESTAMP' }
    end
  end
end
