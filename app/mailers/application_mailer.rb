class ApplicationMailer < ActionMailer::Base
  default from: 'vanloi.infotech@gmail.com'
  layout 'mailer'
end
