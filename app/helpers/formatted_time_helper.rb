module FormattedTimeHelper
  def format_time(time)
     time.blank? ? "" : time.strftime('%Y-%m-%d %I:%M:%S')
  end
end
