module ArticlesHelper
	def img_for_article(article)
  	if article.image?
  		image_tag(article.image.url, height: '40', width: '40')
  	else
  		image_tag "default.jpg"
  	end                
  end
end
