module UsersHelper
  def img_for(user)
  	if user.image?
  		image_tag(user.image.url, height: '200', width: '250', :class => "w3-border")
  	else
  		image_tag "default.jpg"
  	end                
  end
end                  