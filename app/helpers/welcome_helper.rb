module WelcomeHelper
	def img_for(user)
  	if user.image?
  		image_tag(user.image.url, height: '250', width: '200')
  	else
  		image_tag "default.jpg"
  	end                
  end
end
