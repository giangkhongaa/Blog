// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, or any plugin's
// vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require rails-ujs
//= require turbolinks
//= require_tree .

$(document).ready(function() { 
	var myIndex = 0;
	carousel();

	function carousel() {
		var i;
		var x = document.getElementsByClassName("mySlides");
		for (i = 0; i < x.length; i++) {
			x[i].style.display = "none";  
		}
		myIndex++;
		if (myIndex > x.length) {myIndex = 1}    
			x[myIndex-1].style.display = "block";  
    setTimeout(carousel, 10000); // Change image every 2 seconds
}
});

function likeFunction(param, article_id) {

	console.log(article_id);
	$.ajax({
		url: "../welcome/like",
		type:'POST',
		data: {
			article_id: article_id
		},
	})
	.success(function(data) {
		/*console.log("success");*/
		if(data.like) {
			param.innerHTML = "✓ Liked"
		} else {
			param.innerHTML = "<b><i class=\"fa fa-thumbs-up\"></i> Like</b>"
		}
	})
	.error(function(data) {
		console.log("error")
	})
	.done(function(data) {
		/*console.log("done");
		console.log(data);*/
	});
}
function readURL(input) {
	if (input.files && input.files[0]) {
		var reader = new FileReader();

		reader.onload = function (e) {
			$('#blah')
			.attr('src', e.target.result);
		};

		reader.readAsDataURL(input.files[0]);
	}
}

function showComment(id) {
	
	var label = $("#btn-show-" + id).text(); 
	if (label == "Show comments") {
		$("#btn-show-" + id).text("Hidden comments");
		document.getElementById("demo-" + id).style.display='block';
	} else {
		$("#btn-show-" + id).text("Show comments");
		document.getElementById("demo-" + id).style.display='none';
	}

}

function cancelComment(id,articleId) {
	$("#edit-comment-" + id).remove();
	$("#"+ articleId +"-"+id).after("<div id='edit-comment-"+ id +"'> </div>");
}

// START MODAL CONTROL
function hideAllModal() {
	$('.modal').css({ display: "none" });
}

function showModalSignin() {
	hideAllModal();
	$('#modelSignIn').css({ display: "block" });
}

function hideModalSignin() {
	$('#modelSignIn').css({ display: "none" });
}

function showModalSignup() {
	hideAllModal();
	$('#modelSignUp').css({ display: "block" });
}

function hideModalSignup() {
	$('#modelSignUp').css({ display: "none" });
}
// END MODAL CONTROL

function formToJSON(formId) {
	var jsonData = {};
	var formData = $(formId).serializeArray();
	$.each(formData, function() {
		if (jsonData[this.name]) {
			if (!jsonData[this.name].push) {
				jsonData[this.name] = [jsonData[this.name]];
			}
			jsonData[this.name].push(this.value || '');
		} else {
			jsonData[this.name] = this.value || '';
		}

	});
	return jsonData;
}


// START SIGN IN AJAX
function signin(form, e) {
	e.preventDefault();
	$.ajax({
		url: form.action,
		type:'POST',
		beforeSend: function(xhr) {xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'))},
		data: formToJSON('#' + form.id)
	})
	.success(function(data) {
		console.log("success");
		
	})
	.error(function(data) {
		console.log("error")
		$('#error-message-' + form.id).html(data.responseText);
	})
	.done(function(data) {
		/*console.log("done");*/
		console.log(data);
	});
	return false;
}
// END SIGN IN AJAX


