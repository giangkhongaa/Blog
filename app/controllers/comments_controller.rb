class CommentsController < ApplicationController
	before_action :authenticate_user!
	#http_basic_authenticate_with name: "me", password: "me", only: :destroy
	before_action :set_comment, only: [:destroy, :update, :edit]
	load_and_authorize_resource

	def create
		@article = Article.find(params[:article_id])
		@comment = @article.comments.create(comment_params)
		#redirect_to welcome_index_path
		#redirect_to article_path(@article)
		respond_to do |format|
			format.html {redirect_to welcome_index_path @comment}
			format.js {}
           # format.json { render json: @comment}
       end
        #render :json => { :comment => comment}
    end
    def destroy
    	@comment.destroy
    	respond_to do |format|
    		format.html {redirect_to welcome_index_path @comment}
    		format.js {}
    	end
    end
    
    def edit
    	respond_to do |format|
    		format.html {redirect_to welcome_index_path @comment}
    		format.js {}
    	end
    end

    def update
        @article = Article.find(params[:article_id])
    	@comment.update(comment_params)
    	respond_to do |format|
    		format.html {redirect_to welcome_index_path @comment}
    		format.js {}
    	end
    end
	

    private
    def comment_params
    	params.require(:comment).permit(:user_id, :body)
    end

	# Use callbacks to share common setup or constraints between actions.
	def set_comment
		@comment = Comment.find(params[:id])
	end
end
