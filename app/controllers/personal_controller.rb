
class PersonalController < ApplicationController
  helper UsersHelper
  before_action :authenticate_user!
  def index
  end

  def edit
  	@user = current_user
  end
  
  def update
  	@user= current_user
  	@user.update(users_params)
  	redirect_to personal_index_path
  end
  
   # Never trust parameters from the scary internet, only allow the white list through.
    def users_params
      params.require(:user).permit(:id, :first_name, :last_name, :birthday, :image)
    end
end
