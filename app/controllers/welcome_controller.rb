class WelcomeController < ApplicationController
  helper FormattedTimeHelper
  authorize_resource class: WelcomeController

  def index
    @articles = Article.order('created_at DESC')
  end

  def like
    article_id = params[:article_id]

    like = Like.where(user_id: current_user.id, article_id: article_id).take
    liked = false

    if like.nil?
      like.destroy
    else
      like = Like.new(user_id: current_user.id, article_id: article_id)
      like.save
      liked = true
    end

    render json: { like: liked }
  end
end
