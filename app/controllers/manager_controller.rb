class ManagerController < ApplicationController
  before_action :authenticate_user!
  authorize_resource :class => ManagerController
  def index
    @users = Kaminari.paginate_array(User.find_by_sql(["SELECT * FROM USERS WHERE id <> ?", current_user.id])).page(params[:page]).per(5)
  end
  
  def update
    if "admin".eql? params[:roles]
      roles_mask = 1
    else
      roles_mask = 2
    end
    @user = User.find_by_id(params[:user][:id])
    @user.update(roles_mask: roles_mask)
    redirect_to manager_index_path
  end

  def edit
   @user = User.find_by_id(params[:id])
  end

  def user_roles_params
    params.require(:user).permit(:id)
  end
end
