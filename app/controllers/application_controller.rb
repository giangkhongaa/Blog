class ApplicationController < ActionController::Base
  #protect_from_forgery with: :exception
  rescue_from CanCan::AccessDenied do |exception|
        redirect_to root_url, :alert => exception.message
  end
  
  def after_sign_out_path_for(resource)
    new_user_session_path
  end

  before_action :set_work_categories

  rescue_from Exception do |exception|
  	respond_to do |format|
  		format.html { render :file => "error/internal_server_error", 
  			:layout => false, 
  			:status => :internal_server_error, 
  			:locals => {:exception => exception}}
  	end
  end

  def catch_404
  	respond_to do |format|
  		format.html { render :file => "error/page_not_found", 
  			:layout => false, 
  			:status => :not_found, 
  			:locals => {:exception => params[:path]}}
  	end
  end
  
  def set_work_categories
  	#byebug



  	#params[:column] = "sign_in_count) FROM users WHERE last_name = 'Giang';"
	#result = User.calculate(:sum, params[:column])

	#params[:user] = "') or (SELECT 1 AS one FROM `users` WHERE sign_in_count = '4"
	#print User.exists? ["last_name = '#{params[:user]}'"]

  	result = User.where("last_name = '#{params[:name]}'")

  	@popularArticles = Article.limit(4)

  	@inspirationArticles = getInspirationArticles()

  	@tags = Tag.order("name")
  end

  private
	def getPopularArticles
		# query = "SELECT a.id
		# 	FROM articles a
		# 	LEFT JOIN
		# 	    (SELECT article_id,
		# 	            COUNT(article_id) AS COUNT
		# 	     FROM comments
		# 	     GROUP BY article_id) AS B ON B.article_id = a.id
		# 	INNER JOIN users u ON u.id = a.user_id
		# 	WHERE a.created_at >= DATE(NOW() - INTERVAL ? MONTH)
		# 	ORDER BY B.count DESC
		# 	LIMIT ?"

		# number = Constants::POPULAR_ARTICLE_NUMBER
		# month = Constants::POPULAR_ARTICLE_MONTH_AGO

		# st = ActiveRecord::Base.connection.raw_connection.prepare(query)
		# return st.execute(month, number)

		return Article.find_by_sql ["SELECT *
			FROM articles a
			LEFT JOIN
			    (SELECT article_id,
			            COUNT(article_id) AS COUNT
			     FROM comments
			     GROUP BY article_id) AS B ON B.article_id = a.id
			INNER JOIN users u ON u.id = a.user_id
			WHERE a.created_at >= DATE(NOW() - INTERVAL ? MONTH)
			ORDER BY B.count DESC
			LIMIT ?", Constants::POPULAR_ARTICLE_NUMBER, Constants::POPULAR_ARTICLE_MONTH_AGO]
	end

	private
	def getInspirationArticles

		roles_mask = 1
		return Article.find_by_sql ["SELECT * FROM Articles WHERE Articles.ID in (SELECT Users.ID FROM Users WHERE roles_mask = ?) LIMIT 4", roles_mask]
	end


	before_action :configure_permitted_parameters, if: :devise_controller?
	
	def configure_permitted_parameters
		update_attrs = [:first_name, :last_name, :birthday, :address, :religion, :describe, :image, :current_password]
		devise_parameter_sanitizer.permit :account_update, keys: update_attrs
		devise_parameter_sanitizer.permit(:sign_in, keys: [:login, :password, :password_confirmation])
	end


end
