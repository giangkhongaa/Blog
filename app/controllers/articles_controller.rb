class ArticlesController < ApplicationController
  load_and_authorize_resource
  before_action :authenticate_user!

  

	def show
		@article = Article.find(params[:id])
	end

	def index
    if (current_user.roles_mask == 1)
      @articles = Article.all.page(params[:page]).per(5)
    else
      @articles = Article.where(user_id: current_user.id).page(params[:page]).per(5)
    end
  end

  def new
    @article = Article.new
  end

  def edit
    @article = Article.find(params[:id])
  end



  def create
    ActiveRecord::Base.transaction do
     begin
      @article_save = Article.new(article_params)
      @article.user_id = current_user.id
      
      nametag = "rails"

      tag = Tag.where(name: nametag).take

      article_flag = false
      tag_flag = false
      articletag_flag = false

      if (!tag.blank?)
        
        article_flag = @article.save

        articleTag = ArticleTag.new(article_id: @article.id, tag_id: tag.id)
        articletag_flag = articleTag.save
        tag_flag = true
      else
        
        article_flag = @article.save
        
        tag = Tag.new(name: nametag)
        tag_flag = tag.save

        articleTag = Tag.new(article_id: @article.id, tag_id: tag.id)
        articletag_flag = articleTag.save
      end
      
      if (article_flag && tag_flag && articletag_flag)
        redirect_to articles_path
      else
        ActiveRecord::Rollback
        render "new"
      end
      rescue => error
       ActiveRecord::Rollback
       puts error.inspect
       render "new"
     end
   end
 end

   def update
    @article = Article.find(params[:id])

    if @article.update(article_params)
     redirect_to articles_path
   else
     render 'edit'
   end
 end

 def destroy

  ActiveRecord::Base.transaction do
   begin

				# Delete comments
				Comment.where(article_id: params[:id]).destroy_all

				# Delete article
				@article = Article.find(params[:id])
				@article.destroy

				# Back to index
				redirect_to articles_path
			rescue => error
				ActiveRecord::Rollback
				puts error.inspect
			end
		end
	end

	private
	def article_params
		params.require(:article).permit(:title, :text, :image, :user_id)
	end
  private
  def tag_params
    params.require(:tag).permit(:name)
  end
end
