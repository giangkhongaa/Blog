class Article < ApplicationRecord
	has_many :comments
	has_many :like
	has_many :article_tag
	has_many :comments
	belongs_to :user
	
	mount_uploader :image, AvatarUploader

	validates :title, presence: true,
                    length: { minimum: 5 }

    validates :text, presence: true,
                    length: { minimum: 10 }

  after_commit :commit_success, :on => :create

  def commit_success
      puts "commit succses! :D:D"
  end
end
