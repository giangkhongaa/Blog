require_relative 'boot'

require 'rails/all'
require 'carrierwave'
require 'carrierwave/processing/mini_magick'
require 'carrierwave/orm/activerecord'



# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module Blog
  class Application < Rails::Application

  	config.exceptions_app = self.routes

   config.autoload_paths << Rails.root.join('lib')
    # Initialize configuration defaults for originally generated Rails version.
    config.load_defaults 5.1
    config.action_controller.include_all_helpers = true
    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration should go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded.
  end
end
