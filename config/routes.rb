Rails.application.routes.draw do

  get 'errors/not_found'

  get 'errors/internal_server_error'

  get 'manager/index'

  put 'manager/update'

  get 'manager/:id/update', to: 'manager#edit'

  get 'personal/index'

  get 'welcome/index'

  put 'personal/:id/update', to: 'personal#edit', as: 'patient'

  put "/personal/update"

  #get '/comment/:id' => 'comments#destroy'

  post 'welcome/like'
  #resources :statuses
  #root 'welcome#index'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  #resources :articles
  root 'welcome#index'

  resources :articles do
  	resources :comments
  end
  devise_for :users, controllers: {
    :sessions => 'user/sessions',
    :registrations => 'user/registrations',
    :confirmations => 'user/confirmations',
    :omniauth_callbacks => "user/omniauth_callbacks"
  }

  match "*path", to: "application#catch_404", via: :all
end
